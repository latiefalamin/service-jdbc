/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secondstack.training.service;

import com.secondstack.training.core.dao.CategoryDAO;
import com.secondstack.training.core.dao.ProductDAO;
import com.secondstack.training.core.model.Category;
import com.secondstack.training.core.model.Product;
import com.secondstack.training.service.dao.impl.CategoryDAOImpl;
import com.secondstack.training.service.dao.impl.ProductDAOImpl;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * @author Latief
 */
public class Main {

    /**
     * hello
     *
     * @param args the command line arguments
     */
    public static void main(String args[]) throws SQLException {
        System.out.println("=======================================");
        System.out.println("=== Category ==========================");
        System.out.println("=======================================");
        CategoryDAO categoryDAO = CategoryDAOImpl.getInstance();
        
        //save Categories
        Category susu = new Category("Susu");
        Category sembako = new Category("Sembako");
        Category minuman = new Category("Minuman");
        Category cemilan = new Category("Cemilan");
        Category biscuit = new Category("Biscuit");
        categoryDAO.save(susu);
        categoryDAO.save(sembako);
        categoryDAO.save(minuman);
        categoryDAO.save(cemilan);
        categoryDAO.save(biscuit);
        
        //show Categories
        List<Category> categories = categoryDAO.findAll();
        showCategories(categories);

        //update categories
        biscuit = categories.get(4);
        biscuit.setName("Biscuit Coklat");
        categoryDAO.update(biscuit.getId(), biscuit);
        System.out.println("\n=== Category After Update =============\n");
        categories = categoryDAO.findAll();
        showCategories(categories);
        
        //Delete categories
        categoryDAO.delete(biscuit.getId());
        System.out.println("\n=== Category After Delete =============\n");
        categories = categoryDAO.findAll();
        showCategories(categories);
        
        //=====================================================================
        
        System.out.println("\n\n");
        System.out.println("=======================================");
        System.out.println("=== Product ===========================");
        System.out.println("=======================================");
        ProductDAO productDAO = ProductDAOImpl.getInstance();
        
        //save Products
        Product indomilk = new Product("Indomilk", "Susu bubuk Indomilk", 25000d, new Date(2014,2,12), categories.get(0));
        Product cocaCola = new Product("Coca cola", "Minuman bersoda", 5000d, new Date(2014,2,12), categories.get(2));
        Product pocariSweat = new Product("Pocari Sweat", "Minuman ion", 5000d, new Date(2014,2,12), categories.get(2));
        Product qtela = new Product("Qtela", "Keripik singkong", 10000d, new Date(2014,2,12), categories.get(3));
        Product chiki = new Product("Chiki", "Chiki ball", 2500d, new Date(2014,2,12), categories.get(3));
        Product gulaku = new Product("Gulaku", "Gula pasir", 20000d, new Date(2014,2,12), categories.get(1));
        productDAO.save(indomilk);
        productDAO.save(cocaCola);
        productDAO.save(pocariSweat);
        productDAO.save(qtela);
        productDAO.save(chiki);
        productDAO.save(gulaku);
        
        //show products
        List<Product> products = productDAO.findAll();
        showProducts(products);
        
        //update products
        gulaku = products.get(5);
        gulaku.setDescription("Gula Pasir Kemasan");
        gulaku.setPrice(21000d);
        productDAO.update(gulaku.getId(), gulaku);
        System.out.println("\n=== Products After Update =============\n");
        products = productDAO.findAll();
        showProducts(products);
        
        //Delete products
        productDAO.delete(gulaku.getId());
        System.out.println("\n=== Products After Delete =============\n");
        products = productDAO.findAll();
        showProducts(products);
    }

    public static void showCategory(Category category) {
        System.out.println("Category Id : " + category.getId());
        System.out.println("Category Name : " + category.getName());
    }

    public static void showCategories(List<Category> categories) {
        for (Category category : categories) {
            System.out.println();
            showCategory(category);
        }
    }

    public static void showProduct(Product product) {
        System.out.println("Product Id : " + product.getId());
        System.out.println("Product Name : " + product.getName());
        System.out.println("Product Price : " + product.getPrice());
        System.out.println("Product Expired Date : " + product.getExpiredDate());
        System.out.println("Product Descriptio : " + product.getDescription());
        showCategory(product.getCategory());
    }

    public static void showProducts(List<Product> products) {
        for (Product product : products) {
            System.out.println();
            showProduct(product);
        }
    }
}
