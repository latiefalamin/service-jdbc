/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.secondstack.training.service.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Latief
 */
public final class DatabaseConnection {

    private static Connection connection;
    private final static String url = "jdbc:mysql://localhost:3306/jdbc";
    private final static String username = "root";
    private final static String password = "";

    public static Connection getConnection() {
        if (connection == null) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
                connection = DriverManager.getConnection(url, username, password);
            } catch (ClassNotFoundException ex) {
                System.out.println("Database Connection Creation Failed :" + ex.getMessage());
            } catch (SQLException ex) {
                System.out.println("Database Connection Creation Failed :" + ex.getMessage());
            }
        }
        return connection;
    }
}
